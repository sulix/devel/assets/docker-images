# Docker images

Image available here: <br> https://gitlab.com/turkman/devel/assets/docker-images/container_registry

## How to build images
1. install ymp from source
2. download build script
3. run as root like this: `ymp shell build.ympsh --destdir=rootfs`
4. copy image with `docker tag <image-id> <new-name>` command (optional)

## Using image
1. pull from gitlab: `docker pull registry.gitlab.com/turkman/devel/assets/docker-images`
1. create container: `docker run -it registry.gitlab.com/turkman/devel/assets/docker-images /bin/sh`
